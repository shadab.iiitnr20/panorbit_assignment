## The Project has been build by using parcel and only 3 dependencies -> React, ReactDOM, React Router DOM
## The Project has NOT been bootstraped by using create-react-app

Tech Stack Used  -> React, Javascript, TailwindCSS, HTML

## Steps to Launch the Project
1. Download the zip file from GitLab.
2. Extract the zip file and open in Visual Studio Code.
3. In Visual Studio Code, open the terminal and run the command "npm i" to install the required dependencies.
4. Run npm start
5. Open "localhost:1234" in Chrome to access the site.
