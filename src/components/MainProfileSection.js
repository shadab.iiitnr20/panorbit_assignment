//Import necessary dependencies and hooks
import React, { useContext } from "react";
import { useParams, useLocation } from "react-router-dom";
import { DataContext } from "../../utils/ContextProvider";

const tabs = ["posts", "todo", "gallery"]; //Define the tabs array

const MainProfileSection = () => {
  // Use the useLocation hook to get the current active link
  const pathname = useLocation(); //Used useLocation hook to get the pathname and diplay the component condtionally
  const pathnameArray = pathname.pathname.split("/");
  const { usersList } = useContext(DataContext); //Get the usersList using useContext hook from our Context
  const { id } = useParams(); //Used useParams hook to get the user id from the URL

  return (
    <>
    {/* Used a logic to conditonally display the Coming Soon or Main Profile Section Page */}
      {!tabs.includes(pathnameArray[3]) ? (
        <span className="col-span-3">
          <div className=" flex flex-warp justify-between px-8 pb-8 pt-4">
            <div className="p-4 w-[35%]">
              <div>
                <img
                  className="w-40 h-40 rounded-full m-auto "
                  src={usersList[id - 1]?.profilepicture}
                  alt={usersList[id - 1]?.username}
                />
                <p className="text-gray-700 text-xl font-medium pt-2">
                  {usersList[id - 1]?.name}
                </p>
              </div>
              <div className="leading-8  ">
                <p className="text-gray-500 font-medium flex justify-between">
                  Username :{" "}
                  <b className="text-gray-700 text-left">
                    {usersList[id - 1]?.username}
                  </b>
                </p>
                <p className="text-gray-500 font-medium flex justify-between">
                  e-mail :{" "}
                  <b className="text-gray-700 text-left">
                    {usersList[id - 1]?.email}
                  </b>
                </p>
                <p className="text-gray-500 font-medium flex justify-between">
                  Phone :{" "}
                  <b className="text-gray-700 text-left">
                    {usersList[id - 1]?.phone}
                  </b>
                </p>
                <p className="text-gray-500 font-medium flex justify-between">
                  Website :{" "}
                  <b className="text-gray-700 text-left">
                    {usersList[id - 1]?.website}
                  </b>
                </p>
              </div>
              <hr />
              <div className="leading-10 ">
                <p className="text-gray-500 font-medium text-center">Company</p>
                <p className="text-gray-500 font-medium flex justify-between w-[89%]">
                  Name :{" "}
                  <b className="text-gray-700">
                    {usersList[id - 1]?.company.name}
                  </b>
                </p>
                <p className="text-gray-500 font-medium flex justify-between">
                  catchphrase :{" "}
                  <p className="w-[60%]">
                    <b className="text-gray-700 ">
                      {usersList[id - 1]?.company.catchPhrase}
                    </b>
                  </p>
                </p>
                <p className="text-gray-500 font-medium flex justify-between">
                  bs :{" "}
                  <p className="w-[60%]">
                    <b className="text-gray-700 ">
                      {usersList[id - 1]?.company.bs}
                    </b>
                  </p>
                </p>
              </div>
            </div>
            <hr className="border border-gray-300 h-auto" />
            <div className=" w-[50%] p-4 ">
              <div className="leading-8 w-[65%] ">
                <p className="text-gray-500 font-medium text-left">Address :</p>
                <p className="text-gray-500 font-medium flex justify-between">
                  Street :{" "}
                  <b className="text-gray-700">
                    {usersList[id - 1]?.address.street}
                  </b>
                </p>
                <p className="text-gray-500 font-medium flex justify-between">
                  Suite :{" "}
                  <b className="text-gray-700">
                    {usersList[id - 1]?.address.suite}
                  </b>
                </p>
                <p className="text-gray-500 font-medium flex justify-between">
                  City :{" "}
                  <b className="text-gray-700">
                    {usersList[id - 1]?.address.city}
                  </b>
                </p>
                <p className="text-gray-500 font-medium flex justify-between">
                  Zipcode :{" "}
                  <b className="text-gray-700">
                    {usersList[id - 1]?.address.zipcode}
                  </b>
                </p>
              </div>
              <hr />
              <div className="pt-4">
                <img
                  className="rounded-2xl"
                  src="https://static.toiimg.com/thumb/msid-79949586,imgsize-128601,width-400,resizemode-4/79949586.jpg"
                  alt="map"
                />
                <div className="flex justify-end gap-4">
                  <p className="text-gray-500 font-medium flex">
                    Lat:{" "}
                    <p className="text-gray-700">
                      {usersList[id - 1]?.address.geo.lat}
                    </p>
                  </p>
                  <p className="text-gray-500 font-medium flex">
                    Lng:{" "}
                    <p className="text-gray-700">
                      {usersList[id - 1]?.address.geo.lng}
                    </p>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </span>
      ) : (
        <div className="text-center font-bold text-gray-400 text-[5rem] opacity-25 p-40">
          Coming Soon
        </div>
      )}
    </>
  );
};

export default MainProfileSection;
