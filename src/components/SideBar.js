//import necessary dependencies and hooks
import React from "react";
import { Link, useLocation } from "react-router-dom";

const SideBar = () => {
  // Use the useLocation hook to get the current active link
  const pathname = useLocation();
  const pathnameArray = pathname.pathname.split("/");
  const activeTab = pathnameArray[3];
  // Render the side navigation bar with the appropriate links and indicators
  return (
    <nav className="p-8">
      <div className="bg-gradient-to-b from-indigo-500 to-purple-800 p-8 h-[45rem] rounded-3xl">
        <div className="text-left py-44 leading-[4rem] font-semibold">
          <ul>
            {/* Profile link with apt styling */}
            <Link to={`/profile/${pathnameArray[2]}`}> 
              <li
                className={
                  activeTab === undefined ? "text-white" : "text-gray-300"
                }
              >
                Profile
              </li>
            </Link>
            <hr />
            {/* Posts link with apt styling */}
            <Link to={`/profile/${pathnameArray[2]}/posts`}>
              <li
                className={
                  activeTab === "posts" ? "text-white" : "text-gray-300"
                }
              >
                Posts
              </li>
            </Link>
            <hr />
            {/* Gallery link with apt styling */}
            <Link to={`/profile/${pathnameArray[2]}/gallery`}>
              <li
                className={
                  activeTab === "gallery" ? "text-white" : "text-gray-300"
                }
              >
                Gallery
              </li>
            </Link>
            <hr />
            {/* Todo link with apt styling */}
            <Link to={`/profile/${pathnameArray[2]}/todo`}>
              <li
                className={
                  activeTab === "todo" ? "text-white" : "text-gray-300"
                }
              >
                Todo
              </li>
            </Link>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default SideBar;
