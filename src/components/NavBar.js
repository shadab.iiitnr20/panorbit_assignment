import React, { useContext, useState } from "react";
import { DataContext } from "../../utils/ContextProvider";
import { Link, useParams, useLocation } from "react-router-dom";

const Navbar = () => {
  // Use the useLocation hook to get the current active link
  const pathname = useLocation();
  const [openMenu, setOpenMenu] = useState(false);
  const { usersList } = useContext(DataContext);
  const { id } = useParams();

  const handleMenu = () => {
    setOpenMenu(!openMenu);
  };

  return (
    <nav className="flex justify-between border-b p-5">
      {/* Logic To Change the Tab Name in NavBar */}
      <div className="font-bold">
        {pathname.pathname === `/profile/${usersList[id - 1]?.id}`
          ? "Profile"
          : ""}
        {pathname.pathname === `/profile/${usersList[id - 1]?.id}/posts`
          ? "Posts"
          : ""}
        {pathname.pathname === `/profile/${usersList[id - 1]?.id}/gallery`
          ? "Gallery"
          : ""}
        {pathname.pathname === `/profile/${usersList[id - 1]?.id}/todo`
          ? "ToDo"
          : ""}
      </div>
      {/* To diplay the Dropdown */}
      <div>
        <button
          onClick={handleMenu}
          id="dropdownAvatarNameButton"
          data-dropdown-toggle="dropdownAvatarName"
          className="flex items-center text-sm font-medium text-gray-900 rounded-full hover:text-blue-600 dark:hover:text-blue-500 md:mr-0 focus:ring-4 focus:ring-gray-100 dark:focus:ring-gray-700 dark:text-white"
          type="button"
        >
          {/* <span className="sr-only">Open user menu</span> */}
          <img
            className="w-8 h-8 mr-2 rounded-full"
            src={usersList[id - 1]?.profilepicture}
            alt="user photo"
          />
          {usersList[id - 1]?.name}
        </button>
        {openMenu && (
          <div
            id="dropdownAvatarName"
            className="z-10 block p-3 bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700 dark:divide-gray-600 absolute"
          >
            <div className="px-4 py-3 text-sm text-gray-900 dark:text-white">
              <div className="block">
                <img
                  src={usersList[id - 1]?.profilepicture}
                  className="rounded-full w-16 h-16 m-auto"
                  alt={usersList[id - 1]?.name}
                />
                <p className="text-sm my-0.5 font-semibold text-gray-700 text-center">
                  {usersList[id - 1]?.name}
                </p>
                <p className="text-gray-400 my-0.5 text-center break-all">{usersList[id - 1]?.email}</p>
              </div>
            </div>
            {usersList.slice(usersList[id - 1]?.id, usersList[id]?.id).map((user) => (
              <li key={user.id} className=" list-none ">
                <div className="flex justify-center items-center py-[5px] bg-white">
                  <img
                    src={user.profilepicture}
                    alt={user.username}
                    className="w-6 h-6 rounded-full mr-2"
                  />
                  <div className=" text-xs font-medium text-gray-700">{user.name}</div>
                </div>
              </li>
            ))}
            <div className="block m-2 text-center">
              <Link to={"/"}>
                <button className="bg-red-500 text-white font-semibold py-2 px-4 rounded-[2rem] mt-2">
                  Sign out
                </button>
              </Link>
            </div>
          </div>
        )}
      </div>
    </nav>
  );
};
export default Navbar;
