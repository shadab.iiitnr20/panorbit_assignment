import React, { useContext } from "react";
import { Link } from "react-router-dom";
import homePageImg from "../../assets/home.png";
import Loader from "./Loader";
import { DataContext } from "../../utils/ContextProvider";

const LandingPage = () => {
  const { usersList, loading } = useContext(DataContext);

  return (
    //Container Div
    <div
      className="py-12"
      style={{
        backgroundImage: `url(${homePageImg})`,
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center center",
        backgroundSize: "cover",
      }}
    >
      <div //List Container
        className="border-2 w-[34%] m-auto rounded-[3rem]"
        style={{
          boxShadow:
            "rgba(0, 0, 0, 0.19) 0px 10px 20px, rgba(0, 0, 0, 0.23) 0px 6px 6px ",
          maxHeight: "500px",
        }}
      >
        {/* Header Div */}
        <div className=" bg-gray-100 rounded-t-[2.8rem]">
          <h1 className="text-xl text-zinc-600 font-semibold p-8 text-center">
            Select an account
          </h1>
        </div>

        {/* Account Selection List */}
        {/* If the data is loading, show the Loader component */}
        {/* Otherwise, render the list of users */}
        {loading ? (
          <Loader />
        ) : (
          <div
            className="bg-white overflow-y-auto rounded-b-[2.8rem]"
            style={{ maxHeight: "400px" }}
          >
            <div className="p-8 h-[28rem]">
              {usersList?.length > 0 &&
                usersList.map((user) => (
                  <li key={user.id} className="mb-4 list-none">
                    <Link to={`/profile/${user.id}`}>
                      <div className="flex items-center pb-4 bg-white">
                        <img
                          src={user.profilepicture}
                          alt={user.username}
                          className="w-11 h-11 rounded-full mr-2"
                        />
                        <div className="font-medium">{user.name}</div>
                      </div>
                      <hr />
                    </Link>
                  </li>
                ))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default LandingPage;
