import { useRouteError } from "react-router-dom";

///Made the error page component using useRouteError hook from react router dom

const Error = () => {
  const err = useRouteError();
  const { status, statusText } = err;
  return (
    <>
      <div className="m-auto">
        <h1 className="text-3xl font-bold underline">Oopss!!</h1>
        <h2 className="text-3xl font-bold underline">
          Something went wrong..!!
        </h2>
        <h2 className="text-3xl font-bold underline">
          {status + " " + statusText}
        </h2>
      </div>
    </>
  );
};

export default Error;
