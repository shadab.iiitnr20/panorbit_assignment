import React from "react";
//Simple Loader component
const Loader = () => {
  return (
    <div className="flex justify-center items-center text-3xl font-bold underline">
      Loading....!!
    </div>
  );
};

export default Loader;
