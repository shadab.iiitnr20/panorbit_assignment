import React from "react";
import SideBar from "./SideBar";
import MainProfileSection from "./MainProfileSection";
import NavBar from "./NavBar";

//Made a Consice Profile Page by Combining all the Components

const ProfilePage = () => {
  return (
    <div className="grid grid-cols-4 h-20">
      <SideBar className="col-span-1" />
      <div className="col-span-3 mt-6 pr-16 h-[45rem]">
        <NavBar />
        <MainProfileSection />
        {/* ChatBox */}
      </div>
    </div>
  );
};

export default ProfilePage;
