//import all the necessary dependencies, function and componenets
import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider, Outlet } from "react-router-dom";
import LandingPage from "./components/LandingPage";
import Error from "./components/ErrorPage";
import ProfilePage from "./components/ProfilePage";
import ContextProvider from "../utils/ContextProvider";

const App = () => {
  return (
    //Wrapped the root level component with Context Provider Component to pass the data to children component
    <ContextProvider>
      <main>
        <Outlet />
      </main>
    </ContextProvider>
  );
};

//Created the routes using react-router-dom
const appRouter = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        path: "/",
        element: <LandingPage />, //{/* Route for the landing page */}
      },
      {
        path: "/profile/:id",
        element: <ProfilePage />, //{/* Route for the user's profile */}
      },
      {
        path: "/profile/:id/posts",
        element: <ProfilePage />, //{/* Route for the user's posts */}
      },
      {
        path: "/profile/:id/gallery",
        element: <ProfilePage />, //{/* Route for the user's gallery */}
      },
      {
        path: "/profile/:id/todo",
        element: <ProfilePage />, //{/* Route for the user's to-do list */}
      },
    ],
    errorElement: <Error />, //{/* Route for any other URL */}
  },
]);

const rootElement = document.getElementById("root");
const root = ReactDOM.createRoot(rootElement);
root.render(<RouterProvider router={appRouter} />); 
