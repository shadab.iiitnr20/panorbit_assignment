///imported all the necessary hooks and functions

import { createContext, useState, useEffect } from "react";

export const DataContext = createContext({});

const ContextProvider = ({ children }) => {
  const [usersList, setUsersList] = useState([]); //to set the users list
  const [loading, isLoading] = useState(false); // to show loader component if not got response from API

  useEffect(() => {
    getUserList();
  }, []);

  //get response from API
  const getUserList = async () => {
    try {
      isLoading(true);
      const res = await fetch("https://panorbit.in/api/users.json");
      const data = await res.json();
      const { users } = data;
      isLoading(false);
      setUsersList(users);
    } catch (error) {
      console.log(error);
      isLoading(false);
    }
  };

  //Providing the context to the children component . Passing usersList and loading state as props to children compnent
  return (
    <>
      <DataContext.Provider value={{ usersList, loading }}>
        {children}
      </DataContext.Provider>
    </>
  );
};

export default ContextProvider;
